# SNMP UPS

## BatteryStatus

### oid 

`PowerNet-MIB::upsBasicBatteryStatus.0`

### overview

|BatteryStatus| #(int) |
|---|---| 
|unknown|1|
|batteryNormal|2|
|batteryLow|3|
|batteryInFaultCondition| 4|

### DESCRIPTION
The status of the UPS batteries. A batteryLow(3) value
indicates the UPS will be unable to sustain the current
load, and its services will be lost if power is not restored.
The amount of run time in reserve at the time of low battery
can be configured by the upsAdvConfigLowBatteryRunTime.
A batteryInFaultCondition(4)value indicates that a battery
installed has an internal error condition."

## InputLineFailCause"

### oid 

`PowerNet-MIB::upsAdvInputLineFailCause.0`

### overview

|InputLineFailCause| #(int) |
|---|---| 
|noTransfer |1|
|highLineVoltage|2|
|brownout|3|
|blackout|4|
|smallMomentarySag|5|
|deepMomentarySag|6|
|smallMomentarySpike|7|
|largeMomentarySpike|8|
|selfTest|9|
|rateOfVoltageChange|10|

### DESCRIPTION
The reason for the occurrence of the last transfer to UPS
battery power.  The variable is set to:
- noTransfer(1) -- if there is no transfer yet.
- highLineVoltage(2) -- if the transfer to battery is caused
by an over voltage greater than the high transfer voltage.
- brownout(3) -- if the duration of the outage is greater than
five seconds and the line voltage is between 40% of the
rated output voltage and the low transfer voltage.
- blackout(4) -- if the duration of the outage is greater than five
seconds and the line voltage is between 40% of the rated
output voltage and ground.
- smallMomentarySag(5) -- if the duration of the outage is less
than five seconds and the line voltage is between 40% of the
rated output voltage and the low transfer voltage.
- deepMomentarySag(6) -- if the duration of the outage is less
than five seconds and the line voltage is between 40% of the
rated output voltage and ground.  The variable is set to
- smallMomentarySpike(7) -- if the line failure is caused by a
rate of change of input voltage less than ten volts per cycle.
- largeMomentarySpike(8) -- if the line failure is caused by
a rate of change of input voltage greater than ten volts per cycle.
- selfTest(9) -- if the UPS was commanded to do a self test.
- rateOfVoltageChange(10) -- if the failure is due to the rate of change of
the line voltage."

## BatteryNeedsReplacing

###  oid

`PowerNet-MIB::upsAdvBatteryReplaceIndicator.0`

### overview

|BatteryNeedsReplacing| #(int) |
|---|---| 
|no Need replacing |1|
|NEEDS Replacing |2|


## BatteryRunTimeRemaining

###  oid

`PowerNet-MIB::upsAdvBatteryRunTimeRemaining.0`

### DESCRIPTION

The UPS battery run time remaining before battery exhaustion in 0.01 seconds.
