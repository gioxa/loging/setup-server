#!/bin/sh
#
#  nginx.sh
#  Logit
#
#  Created by Danny Goossen on 2021-11-28.
#
# MIT License
#
# Copyright (c) 2021 logit, Gioxa Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
dnf install policycoreutils-python-utils
dnf -y install epel-release
dnf install -y nginx

firewall-cmd --add-service=https
firewall-cmd --runtime-to-permanent
firewall-cmd --reload

curl -o /etc/nginx/conf.d/cockpit.conf -L https://gitlab.com/gioxa/loging/setup-server/-/raw/main/nginx/cockpit.conf
curl -o /etc/nginx/conf.d/grafana.conf -L https://gitlab.com/gioxa/loging/setup-server/-/raw/main/nginx/grafana.conf
curl -o /etc/nginx/conf.d/influxdb.conf -L https://gitlab.com/gioxa/loging/setup-server/-/raw/main/nginx/influxdb.conf
curl -o /etc/nginx/conf.d/minio.conf -L https://gitlab.com/gioxa/loging/setup-server/-/raw/main/nginx/minio.conf
curl -o /etc/nginx/conf.d/nodered.conf -L https://gitlab.com/gioxa/loging/setup-server/-/raw/main/nginx/nodered.conf

systemctl restart nginx

#needed as reverse proxy setup

setsebool -P httpd_can_network_relay 1
setsebool -P httpd_can_network_connect 1

# or more fine grained by creating specific allow profile for nginx

#setenforce Permissive
#grep nginx /var/log/audit/audit.log | audit2allow -M nginx
#semodule -i nginx.pp
#setenforce Enforcing
