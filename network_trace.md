
## tcpdump

interesting info at [danielmiessler.com](https://danielmiessler.com/study/tcpdump/)
with 192.168.10.195 our server and 192.168.1.1 our gateway

```bash
tcpdump -i enp2s0 src 192.168.10.195 and dst 192.168.1.1
```
Output:
```bash
2:08:44.552593 IP nodered-1.34353 > _gateway.domain: 23508+ AAAA? influxdb.ssltbivdc.com. (40)
22:08:44.552689 IP nodered-1.38567 > _gateway.domain: 37458+ A? influxdb.ssltbivdc.com. (40)
22:08:52.759497 IP nodered-1.59428 > _gateway.domain: 52467+ A? grafana.com. (29)
22:08:52.759505 IP nodered-1.36910 > _gateway.domain: 65301+ AAAA? grafana.com. (29)
22:08:53.108599 IP nodered-1.57504 > _gateway.domain: 51315+ AAAA? raw.githubusercontent.com. (43)
22:08:53.108638 IP nodered-1.42500 > _gateway.domain: 15717+ A? raw.githubusercontent.com. (43)
22:08:54.500704 IP nodered-1.38780 > _gateway.domain: 46059+ AAAA? influxdb.ssltbivdc.com. (40)
22:08:54.500712 IP nodered-1.60341 > _gateway.domain: 60462+ A? influxdb.ssltbivdc.com. (40)
22:08:54.553348 IP nodered-1.53664 > _gateway.domain: 20840+ AAAA? influxdb.ssltbivdc.com. (40)
22:08:54.553449 IP nodered-1.59919 > _gateway.domain: 50707+ A? influxdb.ssltbivdc.com. (40)
22:08:57.783240 ARP, Reply nodered-1 is-at 00:17:7c:19:06:5d (oui Unknown), length 28
22:09:04.501151 IP nodered-1.41145 > _gateway.domain: 58405+ AAAA? influxdb.ssltbivdc.com. (40)
22:09:04.501160 IP nodered-1.51607 > _gateway.domain: 64100+ A? influxdb.ssltbivdc.com. (40)
22:09:04.553568 IP nodered-1.56116 > _gateway.domain: 11894+ AAAA? influxdb.ssltbivdc.com. (40)
22:09:04.553672 IP nodered-1.turbonote-2 > _gateway.domain: 39387+ A? influxdb.ssltbivdc.com. (40)
```
and add `-X` to see the packages, 40 bytes with a ?A or ?AAAA is a dns request


## systemtap

from: [How I can identify which process is making UDP traffic on Linux?](https://serverfault.com/a/683327/378763) using 
[systemtap](https://sourceware.org/systemtap/)

### dependencies
```bash
dnf install --enablerepo=baseos-debug kernel-devel-$(uname -r) kernel-debuginfo-$(uname -r) kernel-debuginfo-common-$(uname -m)-$(uname -r)
```

create program to trace who's requesting dns from our dns server
```bash
cat >> udp_detect_domain.stp <<EOF    
probe udp.sendmsg {
  if ( dport == 53 && daddr == "192.168.1.1" ) {
    printf ("PID %5d (%s) sent UDP to %15s 53\n", pid(), execname(), daddr)
  }
EOF
```

### probe

`man probe::udp.sendmsg`

```bash
stap -v udp_detect_domain.stp
```
output:
```bash
Pass 1: parsed user script and 489 library scripts using 288072virt/130420res/12728shr/117316data kb, in 510usr/50sys/551real ms.
Pass 2: analyzed script: 4 probes, 13 functions, 4 embeds, 3 globals using 354164virt/198072res/14496shr/183408data kb, in 3410usr/1020sys/5154real ms.
Pass 3: translated to C into "/tmp/staplLKVrQ/stap_ab837feb626159b490040a0dab979927_21729_src.c" using 354164virt/198200res/14624shr/183408data kb, in 30usr/90sys/118real ms.
Pass 4: compiled C into "stap_ab837feb626159b490040a0dab979927_21729.ko" in 23140usr/3340sys/16457real ms.
Pass 5: starting run.
PID  1222 (telegraf) sent UDP to     192.168.1.1 53
PID  1222 (telegraf) sent UDP to     192.168.1.1 53
...
PID  1234 (grafana-server) sent UDP to     192.168.1.1 53
PID  1234 (grafana-server) sent UDP to     192.168.1.1 53
PID  1234 (grafana-server) sent UDP to     192.168.1.1 53
PID  1234 (grafana-server) sent UDP to     192.168.1.1 53
PID  1234 (grafana-server) sent UDP to     192.168.1.1 53
^CPass 5: run completed in 30usr/70sys/194689real ms.
```

### probe::netfilter.ip.local_out

manual and usage:
```bash
man probe::netfilter.ip.local_out
```

```bash
#! /usr/bin/env stap
# all_trace.stp

# Print a trace of threads sending IP packets (UDP or TCP) to a given
# destination port and/or address.  Default is unfiltered.

global the_dport = 0    # override with -G the_dport=53
global the_daddr = ""   # override with -G the_daddr=127.0.0.1

probe netfilter.ip.local_out {
    if ((the_dport == 0 || the_dport == dport) &&
        (the_daddr == "" || the_daddr == daddr))
	    printf("%s[%d] sent packet to %s:%d %s\n", execname(), tid(), daddr, dport, data_str)
}
```
systemtap all_trace:
```bash
stap -v -m trace_all all_trace.stp -G the_dport=53 -G the_daddr=192.168.1.1
```
output with string data
```bash
telegraf[1459] sent packet to 192.168.1.1:53=> "E\0\0Dl\271@\0@\021@\333\300\250\n\303\300\250\001\001\261\233\05\00\215V\361&\001\0\0\001\0\0\0\0\0\0\binfluxdb\tssltbivdc\003com\0\0\034\0\001"
telegraf[1488] sent packet to 192.168.1.1:53=> "E\0\0Dl\316@\0@\021@\306\300\250\n\303\300\250\001\001\214\354\05\00\215V7\256\001\0\0\001\0\0\0\0\0\0\binfluxdb\tssltbivdc\003com\0\0\034\0\001"
telegraf[1488] sent packet to 192.168.1.1:53=> "E\0\0Dl\317@\0@\021@\305\300\250\n\303\300\250\001\001\302\v\05\00\215V\271h\001\0\0\001\0\0\0\0\0\0\binfluxdb\tssltbivdc\003com\0\0\001\0\001"
grafana-server[2051] sent packet to 192.168.1.1:53=> "E\0\09tw@\0@\0219(\300\250\n\303\300\250\001\001\342z\05\0%\215K\247@\001\0\0\001\0\0\0\0\0\0\agrafana\003com\0\0\001\0\001"
grafana-server[1314] sent packet to 192.168.1.1:53=> "E\0\09tv@\0@\0219)\300\250\n\303\300\250\001\001\325W\05\0%\215K\260~\001\0\0\001\0\0\0\0\0\0\agrafana\003com\0\0\034\0\001"
grafana-server[1314] sent packet to 192.168.1.1:53=> "E\0\0Gt\320@\0@\0218\301\300\250\n\303\300\250\001\001\251\372\05\03\215Y\023z\001\0\0\001\0\0\0\0\0\0\003raw\021githubusercontent\003com\0\0\001\0\001"
grafana-server[2051] sent packet to 192.168.1.1:53=> "E\0\0Gt\317@\0@\0218\302\300\250\n\303\300\250\001\001\256\315\05\03\215Y\216p\001\0\0\001\0\0\0\0\0\0\003raw\021githubusercontent\003com\0\0\034\0\001"
telegraf[4358] sent packet to 192.168.1.1:53=> "E\0\0Dt\333@\0@\0218\271\300\250\n\303\300\250\001\001\u020C\05\00\215VX{\001\0\0\001\0\0\0\0\0\0\binfluxdb\tssltbivdc\003com\0\0\034\0\001"
telegraf[1457] sent packet to 192.168.1.1:53=> "E\0\0Dt\332@\0@\0218\272\300\250\n\303\300\250\001\001\310v\05\00\215V\376\024\001\0\0\001\0\0\0\0\0\0\binfluxdb\tssltbivdc\003com\0\0\001\0\001"
telegraf[4022] sent packet to 192.168.1.1:53=> "E\0\0Du\t@\0@\0218\213\300\250\n\303\300\250\001\001\265{\05\00\215V\327`\001\0\0\001\0\0\0\0\0\0\binfluxdb\tssltbivdc\003com\0\0\034\0\001"
telegraf[4022] sent packet to 192.168.1.1:53=> "E\0\0Du\n@\0@\0218\212\300\250\n\303\300\250\001\001\242t\05\00\215VZ\217\001\0\0\001\0\0\0\0\0\0\binfluxdb\tssltbivdc\003com\0\0\001\0\001"
telegraf[1457] sent packet to 192.168.1.1:53=> "E\0\0D\216\307@\0@\021\036\315\300\250\n\303\300\250\001\001\246\237\05\00\215VLH\001\0\0\001\0\0\0\0\0\0\binfluxdb\tssltbivdc\003com\0\0\001\0\001"
telegraf[1459] sent packet to 192.168.1.1:53=> "E\0\0D\216\310@\0@\021\036\314\300\250\n\303\300\250\001\001\300\\\05\00\215VC\267\001\0\0\001\0\0\0\0\0\0\binfluxdb\tssltbivdc\003com\0\0\034\0\001"
telegraf[1484] sent packet to 192.168.1.1:53=> "E\0\0D\217\006@\0@\021\036\216\300\250\n\303\300\250\001\001\235d\05\00\215V8Q\001\0\0\001\0\0\0\0\0\0\binfluxdb\tssltbivdc\003com\0\0\034\0\001"
telegraf[1484] sent packet to 192.168.1.1:53=> "E\0\0D\217\a@\0@\021\036\215\300\250\n\303\300\250\001\001\254\017\05\00\215VP\320\001\0\0\001\0\0\0\0\0\0\binfluxdb\tssltbivdc\003com\0\0\001

```
