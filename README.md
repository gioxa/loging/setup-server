# setup the logging server

## planning

- Ip-adress and range
- Iternal domain name => `myinternaldomain.com`
- DNS-entries for web applications
- DNS-entries for gateways / dynamic or static DHCP ?
- DNS-entries for UPS / dynamic or static DHCP ?
- DNS-entries for production equipment / dynamic or static DHCP ?
- purchase the domain `myinternaldomain.com` if not already bought
- setup dns on digital ocean=> only needed for certificates
- setup API key for digital ocean

## Basic Server Setup

install minimal setup, [download RockyLinux 8.5 minimal](https://download.rockylinux.org/pub/rocky/8.5/isos/x86_64/Rocky-8.5-x86_64-minimal.iso) and flash to USB.

install with root account and without a user.

### first boot

Server has no network connection, so for the first boot we need a physical connection.

after start-up:
- we prepare network setup, list interfaces:
```bash
nmcli con show
```

![show connection](images/con_show.png)

- check status
```bash
nmcli dev status
```
![device_status](images/device_status.png)


`enp2s0` deducted from above, being our networl interfase
```bash
nmcli con up enp2s0
ip a
```
Please note the ip-address so we can connect remotly and thus we can continue headless.

Type on your local workstation:
```
ssh root@<ipadress>
```
***Note** : *From her onwards, the setup can be finalized through ssh connection, the server can be rendered headless*

#### set our static connection parameters

here we have chosen to set the ipadress to 192.168.1.20 , adjust the network requirement as planned.

```bash
IF=enp2s0
IP=192.168.1.20
GW=192.168.1.1
DNS=192.168.1.1
nmcli con down $IF
nmcli con mod $IF ipv4.addresses $IP/24
nmcli con mod $IF ipv4.gateway $GW
nmcli con mod $IF ipv4.dns “$DNS”
nmcli con mod $IF ipv4.method manual
nmcli con mod $IF con-name ether1
nmcli con mod $IF connection.autoconnect yes
nmcli con up $IF
```

- check if connection up
```
nmcli -f name,autoconnect connection
```
provides a table with devices with the autoconnect status:

![autoconnect](images/autoconnect.png)


### set the hostname of our system

```
hostnamectl set-hostname node-1.myinternaldomain.com
hostnamectl set-hostname --pretty node-1
```

and check the hostname
```
hostnamectl
```

## setup basic security

- a dedicate user for ssh access belonging to sudo-group
- a non standaard port
- don't permit root login
- don't allow password login (only private/public ssh keys)

see script [ssh_user.sh](ssh_user.sh) in this respository

### prepare

before we start, we add some ssh_keys to the autorised keys:

- create directory
```bash
mkdir .ssh
touch  .ssh/authorized_keys
chmod -R 600 .ssh
```
- and add your id_rsa.pub key
```bash
nano  .ssh/authorized_keys
```
now you should be able to **ssh*** into your new server, **without a password**, check in new terminal window!
```bash
ssh root@192.168.1.20
```



### secure

change user name and port as desired before running the script

```bash
export ssh_user=thenewuser;
export ssh_port=8322;

curl -sS -k -L https://gitlab.com/gioxa/loging/setup-server/-/raw/main/ssh_user.sh | bash -
```

**this script:**
- creates a user with name *<ssh_user>*
- set's the password to *<ssh_user>*
- disable root login from ssh
- disables password login
- copies the authorized keys from root, 
- changes the port to *<ssh-port>*
- change the firewall to allow incoming trafic to *<ssh-port>*
- creates a color prompt
- creates a red color promt for root
- updae selinux with new settings
- force passwd update on first logon


to test open a terminal on a different computer and ssh:
```bash
ssh thenewuser@192.168.1.20 -p8322
```

the rest of the actions below we'll do as root user by e.g.:
```bash
sudo -i
```

### time

One important thing with logging and monitoring is to have the correct time, not only for the end server, but for all the equipment and components of the IoT setup.

since we're in Thailand:
```bash
timedatectl set-timezone Asia/Bangkok
```

```bash
yum install chrony
systemctl enable chronyd
```

in `/etc/chrony.conf` change the server section to a local time server, again since we're in Thailand:

```bash
server 0.th.pool.ntp.org
server 1.th.pool.ntp.org
server 2.th.pool.ntp.org
server 3.th.pool.ntp.org
```
and uncomment `allow 192.168.0.0/16` to allow from local network updates and become the time reference for all devices that have no access to external internet such as UPS, gateways, equipment computers...etc.

```bash
systemctl start chronyd
chronyc tracking
chronyc sources
```

open firewall
```bash
firewall-cmd --permanent --add-port=123/udp
firewall-cmd --reload
```

### install `epel repo`

```bash
dnf -y install epel-release
```

### certbot

```
dnf install python3-certbot-dns-digitalocean
```

```bash
mkdir -pv /etc/secrets
touch /etc/secrets/digitalocean.ini
chmod -R  600 /etc/secrets
```

update the `/etc/secrets/digitalocean.ini` with the secret `dns_digitalocean_token = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx`
```bash
nano /etc/secrets/digitalocean.ini
```

```bash
SERVICE_EMAIL=name@mydomain.td
certbot --agree-tos -n -m "$SERVICE_EMAIL" certonly --dns-digitalocean --dns-digitalocean-credentials /etc/secrets/digitalocean.ini --dns-digitalocean-propagation-seconds 120 -d ssltbivdc.com -d '*.ssltbivdc.com'
```

### nginx

All installed services: grafana, influxdb, nodered ... etc are proxy through nginx, and have each there own domain pointing too this server ip.


```bash
curl -sS -k -L https://gitlab.com/gioxa/loging/setup-server/-/raw/main/nginx.sh | bash -
```

### cockpit

```bash
 [root@dlp ~]# systemctl enable --now cockpit.socket
```

`/etc/cockpit/cockpit.conf`:

```toml
[WebService]
Origins = https://cockpit.ssltbivdc.com wss://cockpit.ssltbivdc.com
ProtocolHeader = X-Forwarded-Proto
AllowUnencrypted=true
```

not really needed but good for test:

```bash
firewall-cmd --add-service=cockpit
firewall-cmd --runtime-to-permanent
firewall-cmd --reload
```

after testing, remove firewall rule
```bash
firewall-cmd --permanent --remove-service=cockpit
firewall-cmd --reload
```
and restart to make settings effective
``` bash
 systemctl restart cockpit.socket
```

## setup grafana


```bash
sudo sh -c "cat > /etc/yum.repos.d/grafana.repo" <<'EOF'
[grafana]
name=grafana
baseurl=https://packages.grafana.com/oss/rpm
repo_gpgcheck=1
enabled=1
gpgcheck=1
gpgkey=https://packages.grafana.com/gpg.key
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
EOF

# install
sudo dnf -y install grafana
systemctl enable grafana-server
systemctl start grafana-server
```

##### Package details

- Installs binary to `/usr/sbin/grafana-server`
- Installs systemd service name `grafana-server.service`
- Installs default file (environment vars) to `/etc/sysconfig/grafana-server`
- Copies configuration file to `/etc/grafana/grafana.ini`
    - The default configuration uses a log file at `/var/log/grafana/grafana.log`
    - The default configuration specifies an sqlite3 database at `/var/lib/grafana/grafana.db`


## setup node-red

Select the nodered user when prompted.

```bash
bash <(curl -sL https://gitlab.com/gioxa/loging/linux-installers/-/raw/master/rpm/update-nodejs-and-nodered)
```

---
TODO: check systemd timesync
--- 
```
#sudo sed -i 's!^#ExecStartPre=/bin/bash!ExecStartPre=/bin/bash!;' /etc/systemd/system/nodered.service
sudo systemctl daemon-reload
```

adjust `/etc/node-re/environment` as required and restart service.

#### auth node-red

new hash for password can be created as using the installed nodered, this will prompt for the password to hash.

```bash
node-red admin hash-pw
```

or with local desktop(linux/mac/windows), install node-red-admin, this will prompt for the password to hash.
```bash
npm install -g node-red-admin
node-red-admin hash-pw
```
and uncomment and update in the  `/home/nodered/.node-red/settings.js` the  `adminAuth`
```js
// Securing Node-RED
// -----------------
// To password protect the Node-RED editor and admin API, the following
// property can be used. See http://nodered.org/docs/security.html for details.
adminAuth: {
    sessionExpiryTime: 86400,
    type: "credentials",
    users: [
        {
            username: "admin",
            password: "$2b$08$8QIRmfzlRntebj/m.FIgz.PtbpGug7Lm3GXUQMDPU7.1EnanTHRYS",
            permissions: "*"
        },
        {
            username: "dgoo2308",
            password: "$2b$08$6/wsv4a2J3qh5r/DtWJN5.zL7rMbMVYez2dUqvjuBEABPzOkPy33a",
            permissions: "*"
        }
    ],
     default: {
        permissions: "read"
    }
},
```
**note** *this will give everybody acces to read, only admin to write.*


## influxdb

### setup repository

```bash
cat <<EOF | sudo tee /etc/yum.repos.d/influxdb.repo
[influxdb]
name = InfluxDB Repository - RHEL \$releasever
baseurl = https://repos.influxdata.com/rhel/\$releasever/\$basearch/stable
enabled = 1
gpgcheck = 1
gpgkey = https://repos.influxdata.com/influxdb.key
EOF
```

### install influxdb2.x.x

```bash
dnf install influxdb2
systemctl enable --now influxdb.service
```

### install influx client

```
dnf -y install influxdb2-cli
```

### install install telegraf

```bash
dnf install telegraf
systemctl disable --now telegraf
```

The usualway is to use the `/etc/telegraf/telegraf.conf` file and other custom things in `/etc/telegraf/telegraf.d/*.conf`, this setup starts multiple services, each for their own purpose and bucket for each `/etc/telegraf/telegraf.d/*.conf`.


- create new service file [/etc/systemd/system/telegraf@.service](telegraf@service)
```bash
cat <<EOF | sudo tee /etc/systemd/system/telegraf@.service
[Unit]
Description=The plugin-driven server agent for reporting %i-metrics into InfluxDB
Documentation=https://github.com/influxdata/telegraf
Requires=network.target
After=network-online.target
Wants=network-online.target

[Service]
User=telegraf
EnvironmentFile=-/etc/default/telegraf
ExecStart=/usr/bin/telegraf -config /etc/telegraf/telegraf.d/%i.conf $TELEGRAF_OPTS
ExecReload=/bin/kill -HUP $MAINPID
Restart=on-failure
RestartForceExitStatus=SIGPIPE
KillMode=control-group

[Install]
WantedBy=multi-user.target
EOF
```

- copy the system and ups file in place (repotelegraf.d):
    - [/etc/telegraf/telegraf.d/ups.conf](telegraf.d/ups.conf)
    - [/etc/telegraf/telegraf.d/system.conf](telegraf.d/system.conf)
- enable the ups and system telegraf service
```bash
sudo systemctl enable telegraf@ups.service
sudo systemctl enable telegraf@system.service
```
- Start the new services
```bash
sudo systemctl start telegraf@system.service
sudo systemctl start telegraf@ups.service
```



## fail to Ban

### Install

```bash
dnf install fail2ban
```

### custom jail definition

tutorial from [Protect your system with fail2ban and firewalld blacklists](https://fedoramagazine.org/protect-your-system-with-fail2ban-and-firewalld-blacklists/)

```bash
# cat /etc/fail2ban/jail.local
[DEFAULT]

# "bantime" is the number of seconds that a host is banned.
bantime  = 1d

# A host is banned if it has generated "maxretry" during the last "findtime"
findtime  = 1h

# "maxretry" is the number of failures before a host get banned.
maxretry = 5
```

### create the fail2ban jail for sshd
```bash
# cat /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled = true
```


### enable service
```bash
systemctl enable --now fail2ban
sudo systemctl status fail2ban
```

```bash
fail2ban-client status
```
### chek the loging

```bash
tail -f /var/log/fail2ban.log
```


## system log agregator

with all these systems running, it would be good to agrate all the logging to one server for inspection and followup.

- uncomment in `/etc/rsyslog.conf`
```
module(load="imudp") # needs to be done just once
input(type="imudp" port="514")
```

- and specify just before the `#### GLOBAL DIRECTIVES ####` where to put the logs
```
$template RemoteLogs,"/var/log/remotes/%HOSTNAME%/%PROGRAMNAME%.log"
*.* ?RemoteLogs
& ~
```
-  add the firewall rule and restart the service
```bash
firewall-cmd --permanent --add-port=514/udp
firewall-cmd --reload
systemctl restart rsyslog
```

The Logs are now at: `/var/log/remotes/<hostname>/<program_name>`

#### linux clients setup for logging

append to `/etc/rsyslog.conf`
```
*.*  @192.168.10.195:514
```
and `sudo systemctl restart rsyslog`




